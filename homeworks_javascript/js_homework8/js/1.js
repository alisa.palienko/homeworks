// Создать поле для ввода цены с валидацией.
//
//     Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
//     При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span,
//     в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается
// в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены.
//     Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода
// красной рамкой, под полем выводить фразу - Please enter correct price.
//     span со значением при этом не создается.


const inputPrice = document.createElement("input");
const spanPrice = document.createElement('span');
const containerPrice = document.createElement('div');

spanPrice.innerText = 'Price';
document.body.appendChild(containerPrice);
containerPrice.appendChild(spanPrice);
containerPrice.appendChild(inputPrice);


inputPrice.addEventListener('focus', (e) => {
    const targetInput = e.target;
    targetInput.classList.add('active');
});

inputPrice.addEventListener('blur', (e) => {
    const targetInputBlur = e.target;
    targetInputBlur.classList.remove('active');

    const message = document.createElement('span');

    if (+targetInputBlur.value >= 0) {
        message.innerText = `Текущая цена: ${targetInputBlur.value}`;
        containerPrice.before(message);
        message.classList.add('spanPrice');

        const deleteButton = document.createElement('button');
        deleteButton.innerText = 'X';
        message.appendChild(deleteButton);

        targetInputBlur.style.color = 'green';

        deleteButton.addEventListener('click', () => {
            message.remove();
            targetInputBlur.value = '';
        })
    } else {
        message.innerText = 'Please enter correct price.';
        containerPrice.after(message);
        targetInputBlur.style.borderColor = 'red';
    }

});