// Реализовать функцию, которая будет получать массив элементов и выводить их на
// // страницу в виде списка.
// //
// //     Технические требования:
// //     Создать функцию, которая будет принимать на вход массив.
// //     Каждый из элементов массива вывести на страницу в виде пункта списка
// // Необходимо использовать шаблонные строки и функцию map массива для
// // формирования контента списка перед выведением его на страницу.
// //
// //
// //     Примеры массивов, которые можно выводить на экран:
// //     ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
// //


const arr1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function getElements(arr) {
    const list = document.createElement('ul');
    arr.map(function (element) {
     list.innerHTML += `<li> ${element}</li> `
    });
    document.querySelector('script').before(list);
}
getElements(arr1);
