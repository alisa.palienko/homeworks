// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//
//     Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser())
// и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy)
// и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени
// пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре)
// и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge()
// и getPassword() созданного объекта.


function createNewUser() {
    const name = prompt('Введите имя');
    const surname = prompt('Введите фамилию');
    const birthDate = prompt('Введите год рождения в формате день/месяц/год');

    const birth = birthDate.split('.');
    const date = new Date(+birth[2], +birth[1] - 1, +birth[0]);

    const newUser = {
        firstName: name,
        lastName: surname,
        age: date,


        getAge: function () {
            return new Date().getFullYear() - this.age.getFullYear();

        },
        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.age.getFullYear();
        }
    };
    return newUser;
}

const user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());

