// Реализовать переключение вкладок (табы) на чистом Javascript.
//
// Технические требования:
//
// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку
// отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт
// . В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки
// могут добавляться и удаляться. При этом нужно, чтобы функция, написанная
// в джаваскрипте, из-за таких правок не переставала работать.

const ulMenuNames = document.body.getElementsByClassName('tabs');
const ulContent = document.body.getElementsByClassName('tabs-content');

const liListFirst = ulMenuNames[0].getElementsByTagName('li');
const liListSecond = ulContent[0].getElementsByTagName('li');

for (let u = 0; u <liListSecond.length; u++) {
    liListSecond[u].id = `tab-${u}`;
}

for (let i = 0; i<liListFirst.length; i++) {
    liListFirst[i].dataset.id = `tab-${i}`;
    liListFirst[i].addEventListener('click', function () {
        for (let i = 0; i<liListFirst.length; i++)  {
            liListFirst[i].classList.remove('active')
        }

        for (let u = 0; u <liListSecond.length; u++) {
            if(liListSecond[u].id === this.dataset.id){
                liListSecond[u].classList.add('active')
            } else {
                liListSecond[u].classList.remove('active')
            }
        }


        this.classList.add('active');
    })
}






