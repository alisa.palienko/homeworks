// Написать реализацию кнопки "Показать пароль".
//
//     Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы,
//     которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета
// Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


const form = document.getElementsByClassName('password-form');
console.log(form);

const submitButton = document.querySelector('.btn');
submitButton.addEventListener('click', (event) => {
    event.preventDefault();

    const passFirst = document.querySelector('#pass-first').value;
    const passSecond = document.querySelector('#pass-second').value;
    if (passFirst === passSecond) {
        alert('You are welcome')
    } else {


        const spanError = document.querySelector('.error');
        console.log(spanError);

        if(spanError) {
            console.log('removed');
            spanError.remove();
        }


        const alert = document.createElement('span');
        alert.classList.add('error');
        alert.innerHTML = 'Нужно ввести одинаковые значения';
        document.querySelector('button').before(alert);
        alert.style.color = '#f04a56';

    }

});


const icon1 = document.querySelector('.icon-1');
const icon2 = document.querySelector('.icon-2');

const input1 = document.querySelector('#pass-first');
const input2 = document.querySelector('#pass-second');

icon1.addEventListener('click', () => changeIcon(icon1,input1));

icon2.addEventListener('click', () => changeIcon(icon2,input2));

function changeIcon(icon, input) {
    if (input.type === 'password') {
        input.type = 'text';
        // input1.setAttribute('type', 'text');
        icon.classList.replace('fa-eye-slash', 'fa-eye');
    } else  {
        input.type = 'password';
        icon.classList.replace('fa-eye', 'fa-eye-slash');
    }
}








