//
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser
// со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(),
//     который будет возвращать первую букву имени пользователя,
//     соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser().

//     Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.



function createNewUser() {
    let name = prompt('vvedite firstname');
    let surname = prompt('vvedite lastname');

    const newUser = {
        firstName: name,
        lastName: surname,

        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };

    return newUser;
}


let user = createNewUser();
console.log(newUser.getLogin());


