// Реализовать функцию, которая будет производить математические
// операции с введеными пользователем числами.
//
//     Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера
//     математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.

let a = +prompt('vvedite 1e chislo');
let b = +prompt('vvedite 2e chislo');
let mathOperation = prompt('+, -, *, /');

function validate(a,b) {
   while (Number.isNaN(a) || Number.isNaN(b) || a === '' || b === '') {
        a = +prompt('vvedite 1e chislo');
        b = +prompt('vvedite 2e chislo');
   }
   console.log(count(a,b,mathOperation))
}

function count(a,b,mathOperation) {
    switch (mathOperation) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
}

validate(a,b);