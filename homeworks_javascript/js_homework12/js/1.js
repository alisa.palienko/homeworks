// Реализовать программу, показывающую циклично разные картинки.
//
//     Технические требования:
//
//     В папке banners лежит HTML код и папка с картинками.
//     При запуске программы на экране должна отображаться первая картинка.
//     Через 10 секунд вместо нее должна быть показана вторая картинка.
//     Еще через 10 секунд - третья.
//     Еще через 10 секунд - четвертая.
//     После того, как покажутся все картинки - этот цикл должен начаться заново.
//     При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
//     По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка,
//     которая была там при нажатии кнопки.
//     Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл
// продолжается с той картинки, которая в данный момент показана на экране.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

