function Hamburger(size, stuffing) {

        if (!size) {
            throw new HamburgerException('no size given');
        }
        if (!stuffing) {
            throw new HamburgerException('no stuffing given');
        }
        if (size.type !== "size") {
            throw new HamburgerException('you put not a size');
        }
        if (stuffing.type !== "stuffing") {
            throw new HamburgerException('you put not a stuffing');
        }




    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];

}


Hamburger.SIZE_SMALL = {price: 50, calories: 20, type:"size"};
Hamburger.SIZE_LARGE = {price: 100, calories: 40, type: "size"};
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20, type:"stuffing", name:'cheese'};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5, type:"stuffing", name:'salad'};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10, type:"stuffing", name:'potato'};
Hamburger.TOPPING_MAYO = {price: 20, calories: 5, type:"topping", name:'mayo'};
Hamburger.TOPPING_SPICE = {price: 15, calories: 0, type:"topping", name:'spise'};


Hamburger.prototype.addTopping = function (topping) {
    if (this.toppings.includes(topping)){
        throw new HamburgerException('you tried to dublicate topping')
    }

    if (!topping || topping.type !== 'topping') {
        throw new HamburgerException('its not a topping')
    }
    this.toppings.push(topping)
};




Hamburger.prototype.showSize = function () {
    return this.size
};

Hamburger.prototype.removeTopping = function (topping) {
    if (!this.toppings.includes(topping)){
        throw new HamburgerException('you tried to remove nonexistent topping')
    }

    this.toppings.splice(this.toppings.indexOf(topping),1)
};

Hamburger.prototype.getToppings = function () {
    return this.toppings
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

Hamburger.prototype.calculatePrice = function () {
    let price = this.size.price + this.stuffing.price;
    this.toppings.forEach(topping => {
        price += topping.price
    });
    return price;
};

Hamburger.prototype.calculateCalories = function () {
    let calories = this.size.calories + this.stuffing.calories;
    this.toppings.forEach(topping => {
        calories += topping.calories
    });
    return calories;
};

function HamburgerException (massage) {
    this.name = "HamburgerException ";
    this.massage = massage;
}

let burger;

try {
     burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
} catch (e) {
    console.log(e)
}

try {
    burger.addTopping(Hamburger.STUFFING_CHEESE);
} catch (e) {
    console.log(e)
}

try {
    burger.removeTopping(Hamburger.STUFFING_CHEESE);
} catch (e) {
    console.log(e)
}


console.log(burger);

console.log(burger.calculateCalories());













