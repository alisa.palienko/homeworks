const ulAmazingList = document.body.querySelector('.our-amazing-work-list');
const liAmazingList = ulAmazingList.getElementsByTagName('li');
const buttonLoadMoreStyle = document.body.querySelector("#button-load-more");


for (let i = 0; i < liAmazingList.length; i++) {
    liAmazingList[i].addEventListener('click', function () {
            for (let i = 0; i < liAmazingList.length; i++) {
                liAmazingList[i].classList.remove('active')
            }

            const img = document.body.querySelectorAll('.images > .image');

            for (let i = 0; i < img.length; i++) {
                img[i].classList.remove('filter')
            }

            for (let i = 0; i < img.length; i++) {
                if (this.dataset.filter === 'all') {
                    img[i].classList.add('filter');
                    buttonLoadMoreStyle.style.display= "block"
                }
                if (img[i].dataset.category === this.dataset.filter) {
                    img[i].classList.add('filter');
                    buttonLoadMoreStyle.style.display= "none"

                }
            }

            this.classList.add('active');

        }
    )
}