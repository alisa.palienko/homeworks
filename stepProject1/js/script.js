const ulMenuNames = document.body.getElementsByClassName('services');
const ulContent = document.body.getElementsByClassName('tabs-content');

const liListFirst = ulMenuNames[0].getElementsByTagName('li');
const liListSecond = ulContent[0].getElementsByTagName('li');

for (let u = 0; u <liListSecond.length; u++) {
    liListSecond[u].id = `tab-${u}`;
}

for (let i = 0; i<liListFirst.length; i++) {
    liListFirst[i].dataset.id = `tab-${i}`;

    liListFirst[i].addEventListener('click', function () {
        for (let i = 0; i<liListFirst.length; i++)  {
            liListFirst[i].classList.remove('active')
        }

        for (let u = 0; u <liListSecond.length; u++) {
            if(liListSecond[u].id === this.dataset.id){
                liListSecond[u].classList.add('active')
            } else {
                liListSecond[u].classList.remove('active')
            }
        }


        this.classList.add('active');
    })
}
